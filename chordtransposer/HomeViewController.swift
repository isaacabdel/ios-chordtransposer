//
//  HomeViewController.swift
//  chordtransposer
//
//  Created by Isaac Sanchez on 7/7/16.
//  Copyright © 2016 Isaac Sanchez. All rights reserved.
//

import UIKit

let chords = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B", ]
var selectedChords: [String] = []
var transposedChords: [String] = []
var chordExtensions: [String] = []
var keyValue: Int = 0

var majorIsSelected = true
var minorIsSelected = false
var seventhIsSelected = false

var emptyStringArray: [String] = []

let allChords = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]

let homeViewController = HomeViewController()

class HomeViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(reloadCollectionViewData), name: "reloadData", object: nil)
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(presentAboutView), name: "presentAboutView", object: nil)
        
        self.collectionView?.backgroundColor = UIColor(red: 20/255, green: 20/255, blue: 20/255, alpha: 1)
        
        self.collectionView?.registerClass(SelectedChordCell.self, forCellWithReuseIdentifier: "Cell")
        
        self.collectionView?.contentInset = UIEdgeInsets(top: 24, left: 4, bottom: 0, right: 4)
        
        setupViews()

    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    
    let chordsSelectionView: ChordSelectionView = {
        let view = ChordSelectionView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let chordsControlsView: ChordsControlsView = {
        let view = ChordsControlsView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let chordsExtensionsView: ChordsExtensionsView = {
        let view = ChordsExtensionsView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let statusBarView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.blackColor()
        return view
    }()
    
    func setupViews() {
        
        let chordsSelectionViewHeight = (((view.frame.width) / 6) * 2 ) + 12
        
        collectionView?.frame = CGRectMake(0, 0, view.frame.width, view.frame.height - CGFloat(chordsSelectionViewHeight + 124))
        
        view.addSubview(chordsSelectionView)
        
        view.addSubview(chordsControlsView)
        
        view.addSubview(chordsExtensionsView)
        
        view.addSubview(statusBarView)
        
        statusBarView.frame = CGRectMake(0, 0, view.frame.width, 20)
        
        
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[view]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["view": chordsSelectionView]))
        
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[chordsControlsView]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["chordsControlsView": chordsControlsView]))
        
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[chordsExtensionsView]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["chordsExtensionsView": chordsExtensionsView]))
        
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[view(\(chordsSelectionViewHeight + 60))]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["view": chordsSelectionView]))
        
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[chordsExtensionsView(60)]-\(chordsSelectionViewHeight + 60)-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["chordsExtensionsView": chordsExtensionsView]))
        
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[chordsControlsView(60)]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["chordsControlsView": chordsControlsView]))
        
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedChords.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as? SelectedChordCell
        cell?.selectedChordLabel.text = selectedChords[indexPath.row] + chordExtensions[indexPath.row]
        
        if (transposedChords.count != 0) {
            cell?.transposedChordLabel.text = transposedChords[indexPath.row] + emptyStringArray[indexPath.row]
        }
        
        cell!.backgroundColor = UIColor(red: 60/255, green: 159/255, blue: 211/255, alpha: 1)
        cell!.layer.cornerRadius = 4
        
        return cell!
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake((self.view.frame.width / 4) - 4 , self.view.frame.width / 4)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 4
    }
    
    func triggerReloadDataFunction() {
        NSNotificationCenter.defaultCenter().postNotificationName("reloadData", object: nil)
    }
    
    func reloadCollectionViewData() {
        collectionView?.reloadData()
    }
    
    func triggerPresentAboutViewFunction() {
        NSNotificationCenter.defaultCenter().postNotificationName("presentAboutView", object: nil)
    }
    
    func presentAboutView() {
        let aboutView = AboutViewController()
        presentViewController(aboutView, animated: true, completion: nil)
    }


}

class ChordSelectionView: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    let chordsCollectionView: UICollectionView = {
        let view = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
        view.backgroundColor = UIColor.blackColor()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    func setupViews() {
        
        chordsCollectionView.registerClass(ChordCell.self, forCellWithReuseIdentifier: "Cell")
        
        addSubview(chordsCollectionView)
        chordsCollectionView.contentInset = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
        
        chordsCollectionView.delegate = self
        chordsCollectionView.dataSource = self
        
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[view]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["view": chordsCollectionView]))
        
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[view]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["view": chordsCollectionView]))
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as? ChordCell
        cell?.chordLabel.text = chords[indexPath.row]
        cell?.layer.cornerRadius = 4
        cell!.backgroundColor = UIColor(red: 20/255, green: 20/255, blue: 20/255, alpha: 1)
        
        return cell!
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        addChordToSelectedChords(chords[indexPath.row])
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake((frame.width / 6) - 4 , frame.width / 6)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 4
    }
    
    func addChordToSelectedChords(chord: String) {
        if majorIsSelected == true {
            chordExtensions.append("")
        }
        if minorIsSelected == true {
            chordExtensions.append("m")
        }
        if seventhIsSelected == true {
            chordExtensions.append("7")
        }
        
        emptyStringArray.append("")
        selectedChords.append(chord)
        transposedChords.append("")
        homeViewController.triggerReloadDataFunction()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class ChordsExtensionsView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    let majorButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = UIColor(red: 74/255, green: 179/255, blue: 231/255, alpha: 1)
        button.setTitle("M", forState: .Normal)
        button.addTarget(nil, action: #selector(selectMajorButton), forControlEvents: .TouchUpInside)
        return button
    }()
    
    let minorButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = UIColor(red: 74/255, green: 179/255, blue: 231/255, alpha: 1)
        button.setTitle("m", forState: .Normal)
        button.addTarget(nil, action: #selector(selectMinorButton), forControlEvents: .TouchUpInside)
        return button
    }()
    
    let seventhButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = UIColor(red: 74/255, green: 179/255, blue: 231/255, alpha: 1)
        button.setTitle("7", forState: .Normal)
        button.addTarget(nil, action: #selector(selectSeventhButton), forControlEvents: .TouchUpInside)
        return button
    }()
    
    let aboutButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = UIColor(red: 67/255, green: 101/255, blue: 253/255, alpha: 1)
        button.addTarget(nil, action: #selector(presentAboutView), forControlEvents: .TouchUpInside)
        button.setTitle("⚙", forState: .Normal)
        return button
    }()
    
    func setupViews() {
        
        
        addSubview(majorButton)
        addSubview(minorButton)
        addSubview(seventhButton)
        addSubview(aboutButton)
        
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[majorButton][minorButton(==majorButton)][seventhButton(==majorButton)][aboutButton(==majorButton)]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["majorButton": majorButton, "minorButton": minorButton, "seventhButton": seventhButton, "aboutButton": aboutButton]))
        
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[majorButton]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["majorButton": majorButton]))
        
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[minorButton]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["minorButton": minorButton]))
        
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[seventhButton]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["seventhButton": seventhButton]))
        
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[aboutButton]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["aboutButton": aboutButton]))
        
        majorButton.backgroundColor = UIColor(red: 60/255, green: 159/255, blue: 211/255, alpha: 1)
    }
    
    func clearButtonSelections() {
        majorButton.backgroundColor = UIColor(red: 74/255, green: 179/255, blue: 231/255, alpha: 1)
        minorButton.backgroundColor = UIColor(red: 74/255, green: 179/255, blue: 231/255, alpha: 1)
        seventhButton.backgroundColor = UIColor(red: 74/255, green: 179/255, blue: 231/255, alpha: 1)
        
        majorIsSelected = false
        minorIsSelected = false
        seventhIsSelected = false
        
        
    }
    
    func selectMajorButton() {
        clearButtonSelections()
        majorButton.backgroundColor = UIColor(red: 60/255, green: 159/255, blue: 211/255, alpha: 1)
        majorIsSelected = true
    }
    
    func selectMinorButton() {
        clearButtonSelections()
        minorButton.backgroundColor = UIColor(red: 60/255, green: 159/255, blue: 211/255, alpha: 1)
        minorIsSelected = true
    }
    
    func selectSeventhButton() {
        clearButtonSelections()
        seventhButton.backgroundColor = UIColor(red: 60/255, green: 159/255, blue: 211/255, alpha: 1)
        seventhIsSelected = true
    }
    
    func presentAboutView() {
        homeViewController.triggerPresentAboutViewFunction()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class ChordsControlsView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    let transposeButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = UIColor(red: 13/255, green: 51/255, blue: 79/255, alpha: 1)
        button.setTitle("✅", forState: .Normal)
        button.addTarget(nil, action: #selector(transposeChords), forControlEvents: UIControlEvents.TouchUpInside)
        return button
    }()
    
    let clearButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = UIColor(red: 13/255, green: 51/255, blue: 79/255, alpha: 1)
        button.setTitle("❌", forState: .Normal)
        button.addTarget(nil, action: #selector(clearSelectedChords), forControlEvents: UIControlEvents.TouchUpInside)
        return button
    }()
    
    let raiseKeyButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = UIColor(red: 13/255, green: 51/255, blue: 79/255, alpha: 1)
        button.setTitle("👆", forState: .Normal)
        button.addTarget(nil, action: #selector(raiseKeyValueByOne), forControlEvents: UIControlEvents.TouchUpInside)
        return button
    }()
    
    let lowerKeyButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = UIColor(red: 13/255, green: 51/255, blue: 79/255, alpha: 1)
        button.setTitle("👇", forState: .Normal)
        button.addTarget(nil, action: #selector(lowerKeyValueByOne), forControlEvents: UIControlEvents.TouchUpInside)
        return button
    }()
    
    let keyLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = String(keyValue)
        label.font = UIFont.systemFontOfSize(22)
        label.textColor = UIColor.whiteColor()
        label.textAlignment = .Center
        label.backgroundColor = UIColor(red: 24/255, green: 93/255, blue: 145/255, alpha: 1)
        
        return label
    }()
    
    func setupViews() {
        addSubview(transposeButton)
        addSubview(clearButton)
        addSubview(raiseKeyButton)
        addSubview(lowerKeyButton)
        addSubview(keyLabel)
        
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[clearButton(==transposeButton)][lowerKeyButton(==transposeButton)][keyLabel(==transposeButton)][raiseKeyButton(==transposeButton)][transposeButton]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["transposeButton": transposeButton, "clearButton": clearButton, "raiseKeyButton": raiseKeyButton, "lowerKeyButton": lowerKeyButton, "keyLabel": keyLabel]))
        
        
        
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[transposeButton]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["transposeButton": transposeButton]))
        
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[clearButton]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["clearButton": clearButton]))
        
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[raiseKeyButton]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["raiseKeyButton": raiseKeyButton]))
        
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[lowerKeyButton]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["lowerKeyButton": lowerKeyButton]))

        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[keyLabel]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["keyLabel": keyLabel]))

    }
    
    func transposeChords() {
        
        emptyStringArray = chordExtensions
        
        
        transposedChords = []
        
        for chord in selectedChords {
            
            let index = chords.indexOf(chord)
            transposedChords.append(allChords[12 + index! + keyValue])
            
        }
        homeViewController.triggerReloadDataFunction()
        
        
    }
    
    func clearSelectedChords() {
        emptyStringArray = []
        selectedChords = []
        transposedChords = []
        chordExtensions = []
        homeViewController.triggerReloadDataFunction()
    }
    
    func raiseKeyValueByOne() {
        if keyValue == 11 {
            
        } else {
            keyValue = keyValue + 1
            keyLabel.text = String(keyValue)
        }
        
    }
    
    func lowerKeyValueByOne() {
        if keyValue == -11 {
            
        } else {
            keyValue = keyValue - 1
            keyLabel.text = String(keyValue)
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class SelectedChordCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
    }
    
    let selectedChordLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = ""
        label.textColor = UIColor(red: 20/255, green: 20/255, blue: 20/255, alpha: 1)
        label.font = UIFont.systemFontOfSize(14)
        label.textAlignment = .Center
        return label
    }()
    
    let transposedChordLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = ""
        label.textColor = UIColor.whiteColor()
        label.font = UIFont.systemFontOfSize(30)
        label.textAlignment = .Center
        return label
    }()
    
    func setUpView() {
        
        addSubview(selectedChordLabel)
        addSubview(transposedChordLabel)
        
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-8-[label]", options: NSLayoutFormatOptions(), metrics: nil, views: ["label": selectedChordLabel]))
        
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-8-[label]", options: NSLayoutFormatOptions(), metrics: nil, views: ["label": selectedChordLabel]))
        
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[label]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["label": transposedChordLabel]))
        
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[label]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["label": transposedChordLabel]))
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}


class ChordCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
    }
    
    let chordLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = ""
        label.textColor = UIColor.whiteColor()
        label.font = UIFont.systemFontOfSize(30)
        label.textAlignment = .Center
        return label
    }()
    
    func setUpView() {
        
        addSubview(chordLabel)
        
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[chordLabel]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["chordLabel": chordLabel]))
        
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[chordLabel]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["chordLabel": chordLabel]))
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
