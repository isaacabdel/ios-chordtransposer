//
//  AboutViewController.swift
//  chordtransposer
//
//  Created by Isaac Sanchez on 7/11/16.
//  Copyright © 2016 Isaac Sanchez. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor(red: 13/255, green: 51/255, blue: 79/255, alpha: 1)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissView))
        view.addGestureRecognizer(tap)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissView() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

}
